package flat

import java.awt.Color
import java.awt.Graphics2D
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO


class Flat(val g: Graphics2D) {
    companion object {
        val hImage = 900
        val vImage = 700
    }

    val hEntry = 295
    val vEntry = 190

    val hKitc = 246
    val vKitc = 421

    val huBath = 216
    val vlBath = 193
    val vrBath = 208
    val hdBath = 159

    val huMain = 575
    val hdMain = 261
    val vrMain = 377
    val vlMain = 228
    val vlMainEntry = 18
    val vlMainZkt = vrMain - vlMainEntry - vlMain
    val hdMainZkt = huMain - hEntry - hdMain

    val stenaFat = 35
    val stena = 14

    fun draw() {
        rct(0, 0, hImage, vImage, Color.DARK_GRAY)
        g.translate(stenaFat, stenaFat)
        roomMain()
        roomEntry()
        roomKitc()
        roomBath()
        windows()
        doors()
        table()
        //table2()
        bed()
    }

    fun roomMain() {
        val c = Color(20, 100, 200)
        val area = Rect(0, 0, huMain, vlMainZkt).draw(c).area +
                Rect(hdMainZkt, vlMainZkt, huMain - hdMainZkt, vlMain).draw(c).area +
                Rect(hdMainZkt + hdMain, vrMain - vlMainEntry, hEntry, vlMainEntry).draw(c).area
        drawArea(area, huMain / 2, vrMain / 2)
    }

    fun roomEntry() {
        val c = Color(20, 140, 170)
        Rect(hdMainZkt + hdMain, stena + vrMain, hEntry, vEntry).draw(c).drawArea()
        // door
        Rect(hdMainZkt + hdMain+7, stena + vrMain+vEntry, 98, stenaFat).draw(Color.GRAY)
    }

    fun roomKitc() {
        val c = Color(190, 180, 50)
        Rect(stena + huMain, stena + vrMain + vEntry - vKitc, hKitc, vKitc).draw(c).drawArea()
        // window
        Rect(stenaFat + huMain, stena + vrMain + vEntry - vKitc-stenaFat, 152, stenaFat).draw(Color.GRAY.brighter())
    }

    fun roomBath() {
        val c = Color(200, 100, 75)
        tran(hdMain + hdMainZkt - huBath - stena,vrMain + stena - vlMainEntry) {
            val area = Rect(0, 0, huBath, vlBath).draw(c).area +
                    Rect(huBath - hdBath, vlBath, hdBath, vrBath - vlBath).draw(c).area
            drawArea(area, huBath / 2, vlBath / 2)


            Rect(0, 0, 180, 80).draw(Color(110, 110, 150))
            Rect(0, 0, 170, 70).draw(Color(120, 120, 180))
        }
    }

    fun windows() {
        Rect(90, -stenaFat, 152, stenaFat).draw(Color.GRAY.brighter())
        Rect(90 + 152 + 90, -stenaFat, 152, stenaFat).draw(Color.GRAY.brighter())
    }

    fun doors() {
        val c = Color.GRAY
        Rect(hdMainZkt + hdMain + 75, vrMain, 128, stena).draw(c)
        Rect(hdMainZkt + hdMain - stena, stena + vrMain + 49, stena, 77).draw(c)
        Rect(hdMainZkt + hdMain +hEntry, stena + vrMain, stena, 90).draw(c)
    }

    fun table() {
        safeTran {
            g.translate(hdMainZkt+160, vlMainZkt-100)
            g.rotate(Math.PI/2)

            rct(0, 0, 110, 50, Color.BLACK)
            rct(110 - 60, 0, 60, 160, Color.BLACK)
        }

    }

    fun table2() {
        rct(50, 0, 80, 160, Color(20, 45, 200, 170))
    }

    fun bed() {
        //rct(hdMainZkt + 50, vrMain - vlMainEntry - 200, 180, 200, Color(170, 200, 30))
        //rct(hdMainZkt + 50, vrMain - vlMainEntry - 190, 160, 190, Color(170, 200, 30).brighter())

        rct(hdMainZkt, vrMain - vlMainEntry - 180, 200, 180, Color(170, 200, 30))
        rct(hdMainZkt, vrMain - vlMainEntry - 160, 190, 160, Color(170, 200, 30).brighter())
    }

    fun rct(x: Int, y: Int, w: Int, h: Int, c: Color) {
        g.color = c
        g.fillRect(x, y, w, h)
    }

    fun drawArea(v: Int, x: Int, y: Int) {
        g.color = Color.WHITE
        g.drawString((v / 10000.toDouble()).toString(), x, y)
    }

    fun safeTran(fn: () -> Unit) {
        val t = g.transform
        fn()
        g.transform = t
    }

    fun tran(x:Int,y:Int,fn: () -> Unit) {
        val t = g.transform
        g.translate(x,y)
        fn()
        g.transform = t
    }


    inner class Rect(val x: Int, val y: Int, val xr: Int, val yr: Int) {
        fun draw(c: Color): Rect {
            g.color = c
            g.fillRect(x, y, xr, yr)
            return this
        }

        val area = xr * yr

        fun drawArea() {
            drawArea(area, x + xr / 2, y + yr / 2)
        }
    }
}


fun main(args: Array<String>) {
    val image = BufferedImage(Flat.hImage, Flat.vImage, BufferedImage.TYPE_INT_RGB)
    Flat(image.createGraphics()).draw()
    ImageIO.write(image, "png", File("flat.png"))
}

