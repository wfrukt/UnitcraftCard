package cardserver

import cardclient.Snapshot
import org.json.simple.JSONAware
import org.json.simple.JSONObject
import org.json.simple.JSONStreamAware
import org.json.simple.JSONValue
import java.util.*

class Game{
    val voins = HashMap<Side, Voin>()

    init{
        voins[Side.a] = Voin(2)
        voins[Side.b] = Voin(6)

        initDeck(voins[Side.a]!!.deck)
        initDeck(voins[Side.b]!!.deck)

        voins[Side.a]!!.draw(5)
        voins[Side.b]!!.draw(6)
    }

    fun snapshot():String{
        val snap = JSONObject()
        snap["hand"] = voins[Side.a]!!.hand
        snap["deck"] = voins[Side.a]!!.deck
        snap["discard"] = voins[Side.a]!!.pile
        snap["board"] = snapBoard()
        return snap.toJSONString()
    }

    fun initDeck(deck:Deck){
        deck.add(Card.sweep)
        deck.add(Card.assault)
        deck.add(Card.cross)
        deck.add(Card.dive)
        deck.add(Card.focus)
        deck.add(Card.grasp)
        deck.add(Card.spike)
        deck.add(Card.guard)
        deck.shuffle()
    }

    fun prepare(){
        voins[Side.a]!!.draw(2)
    }

    fun strike(idxCard:Int){
        voins[Side.a]!!.discard(idxCard)
    }

    fun strikeWild(){

    }

    private fun snapBoard():List<String>{
        val board = ArrayList<String>()
        repeat(9){
            board.add(if(voins[Side.a]!!.pst==it) "You" else if(voins[Side.b]!!.pst==it) "Opp" else "")
        }
        return board
    }
}

class Voin(var pst:Int){
    var life = 30
    val deck = Deck()
    val hand = Hand()
    val pile = Pile()

    fun draw(qnt:Int){
        //repeat(qnt) { hand.cards.add(deck.cards.remove(0)) }
    }

    fun discard(idxCard:Int){
        //pile.cards.add(hand.cards.remove(idxCard))
    }
}

class Hand:JSONAware{
    val cards = ArrayList<Card>()
    override fun toJSONString() = cards.toJSONString()
}

class Pile :JSONAware{
    val cards = ArrayList<Card>()
    override fun toJSONString() = cards.toJSONString()
}

class Card(val id:String):JSONAware {
    override fun toJSONString() = JSONValue.toJSONString(id)

    companion object{
        val sweep = Card("Sweep")
        val spike = Card("Spike")
        val assault = Card("Assault")
        val guard = Card("Guard")
        val grasp = Card("Grasp")
        val focus = Card("Focus")
        val cross = Card("Cross")
        val dive = Card("Dive")
    }
}


class Deck:JSONAware{
    val cards = ArrayList<Card>()

    fun shuffle(){
        Collections.shuffle(cards)
    }

    fun add(card:Card){
        cards.add(card)
        cards.add(card)
    }

    override fun toJSONString() = cards.toJSONString()
}

fun List<JSONAware>.toJSONString() = JSONValue.toJSONString(this)

enum class Side{
    a,b
}