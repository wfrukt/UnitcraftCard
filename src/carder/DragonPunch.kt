package carder

val dragonpunch = cardset("dragonpunch", Dmn(630, 880), Dmn(6, 3)) {

    card("block", 2)
    card("fireball", 2)
    card("highKick", 2)
    card("lowPoke", 2)
    card("throw", 2)
    card("taunt", 2)

    card("blue", 1)
    card("cyan", 1)
    card("grey", 1)
    card("magenta", 1)
    card("red", 1)
    card("white", 1)
    card("yellow", 1)
}