package carder

import java.awt.Font
import java.awt.font.TextAttribute
import java.text.AttributedString
import java.util.*

val exceed = cardset("exceed", Dmn(630, 880), Dmn(6, 3)) {

    class AideBody(val card: Card) {
        val list = ArrayList<Pair<String, String>>()
        fun before(txt: String) = desc("Before: ", txt)

        fun after(txt: String) = desc("After: ", txt)

        fun hit(txt: String) = desc("Hit: ", txt)

        fun allways(txt: String) {
            desc("", txt)
        }

        fun desc(prefix: String, txt: String) {
            list.add(prefix to txt)
        }
    }

    fun Card.body(fn: AideBody.() -> Unit) {
        txtAttr(40, 560, 630 - 40 * 2, 760 - 560,
                AideBody(this).apply { fn() }.list.map {
                    val (prefix, txt) = it
                    AttributedString(prefix + txt).apply {
                        addAttribute(TextAttribute.FONT, Font("arial", Font.BOLD, 40))
                        if(prefix.isNotBlank()) addAttribute(TextAttribute.FONT, Font("verdana", Font.BOLD, 40), 0, prefix.lastIndex)
                    }
                })
    }

    fun Card.boost(txt: String) {
        return txtAttr(70, 775, 630 - 70, 880 - 775, AttributedString(txt).apply {
            addAttribute(TextAttribute.FONT, Font("arial", Font.BOLD, 36))
        })
    }


    card("base/assault", 6)
    card("base/dive", 6)
    card("base/focus", 6)
    card("base/cross", 6)
    card("base/grasp", 6)
    card("base/block", 6)
    card("base/spike", 6)
    card("base/sweep", 6)

    card("alice/alice", 1)
    card("alice/aliceEx", 1)
    card("alice/bloodyBaptism", 2)
    card("alice/crossBlades", 2)
    card("alice/darkCorruption", 2)
    card("alice/dualBladeRapture", 2)
    card("alice/soulGazer", 2)
    card("alice/surprisePunishment", 2)
    card("alice/swordCross", 2)

    card("reese/reese", 1)
    card("reese/reeseEx", 1)
    card("reese/Ballista", 2)
    card("reese/Checkmate", 2)
    card("reese/Chivalry", 2)
    card("reese/GallantDefender", 2)
    card("reese/GaunletFlurry", 2)
    card("reese/KnightWave", 2)
    card("reese/SovereignGlory", 2)

    card("skull/skull", 1)
    card("skull/skullEx", 1)
    card("skull/Bang", 2)
    card("skull/BingBong", 2)
    card("skull/Kaplow", 2)
    card("skull/SlamEvil", 2)
    card("skull/Splam", 2)
    card("skull/Zing", 2)
    card("skull/Zzzap", 2)

    card("luciay/luci", 1)
    card("luciay/luciEx", 1)
    card("luciay/BugZapper", 2)
    card("luciay/Downburst", 2)
    card("luciay/FireflyGunner", 2)
    card("luciay/MantisStrike", 2)
    card("luciay/RideLighthing", 2)
    card("luciay/SkiesAflame", 2)
    card("luciay/TalonSweep", 2)

    card("nehtali/nehtali", 1)
    card("nehtali/nehtaliEx", 1)
    card("nehtali/heavenPunishment", 2) {
        body {
            before("+2 Power for each card in opponent's Gauge")
            after("Move 1")
        }
        boost("If you are at Range 1, push the opponent 3")
    }
    card("nehtali/azazelTorment", 2) {
        body {
            before("+3 Power if your opponent has 3 or more Gauge")
            hit("Push 3")
        }
        boost("Retreat 2, then Strike")
    }
    card("nehtali/DarknessBarrier", 2) {
        body {
            allways("Attacks at Range 4 do not hit you")
        }
        boost("Your opponent discards 1 Gauge. Add this card to your Gauge")
    }
    card("nehtali/Hellfire", 2) {
        body {
            hit("+1 Power for each card in your Gauge")
        }
        boost("Add this card to your Gauge")
    }

    card("nehtali/HellSalvation", 2) {
        boost("Draw 2. Add one of those cards to your Gauge")
    }
    card("nehtali/SoulTresher", 2) {
        body {
            before("You may spend 1 Gauge for +2 Power")
        }
        boost("Move the opponent 1, then Strike")
    }
    card("nehtali/VoltDamnation", 2) {
        body {
            allways("Ignore Armor")
        }
        boost("+4 Speed")
    }

    card("vincent/vincent", 1)
    card("vincent/vincentEx", 1)
    card("vincent/BallotFixing", 2) {
        body {
            allways("Ignore Guard")
            hit("Gain Advantage")
        }
        boost("Draw until you have 5 cards in hand")
    }
    card("vincent/CrimsonBarrage", 2) {
        body {
            hit("Name 2 cards. Opponent discards all copies of those cards, then reveals hand")
        }
        boost("Draw until you have 3 cards in hand")
    }
    card("vincent/GatlingPunch", 2) {
        body {
            hit("Spend up to 7 Force for +1 Power per Force spent")
        }
        boost("+5 Guard and Strike")
    }
    card("vincent/MajorityWhip", 2) {
        body {
            hit("Push the opponent as far as possible")
        }
        boost("Close 2 and +2 Speed")
    }
    card("vincent/NationalGuard", 2) {
        body {
            before("Close 1 for each point of damage you have taken this turn.")
        }
        boost("Ignore Guard and draw a card")
    }
    card("vincent/PhoenixAscent", 2) {
        body {
            hit("Push 1")
        }
        boost("+2 Armor, and opponents cannot move you.")
    }
    card("vincent/PhoenixRevival", 2) {
        body {
            hit("Regain 4 life")
        }
        boost("Spend up to 5 Force to regain that much life")
    }
}