package carder

import resize
import rotate
import saveJpg
import java.awt.Color
import java.awt.Graphics2D
import java.awt.RenderingHints
import java.awt.font.LineBreakMeasurer
import java.awt.font.TextLayout
import java.io.File
import java.text.AttributedString
import java.util.*
import javax.imageio.ImageIO
import kotlin.properties.Delegates
import java.awt.image.BufferedImage as Image

fun generate(output: String, vararg cardset: Cardset) {
    Pager(Dmn(4060,3050), Dmn(4795,3602), output, Color.GREEN).generate(cardset.toList())
}

class AideCardset(val pager: Pager, val cardset: Cardset) {

    fun card(nameImgBg: String, qnt: Int, fn: (Card.() -> Unit)? = null) {
        val dmnPxCard = pager.dmnPxFromDmnMm(cardset.dmnCard)
        println(File("data/" + cardset.name, nameImgBg+".png"))
        val imgBg = resize(ImageIO.read(File("data/" + cardset.name, nameImgBg+".png")), dmnPxCard.x, dmnPxCard.y, true)
        repeat(qnt) {
            pager.addCard { g->
                g.drawImage(imgBg, 0, 0, null)
                if (fn != null) Card(g,pager).fn()
            }
        }

    }
}

class Pager(val dmnMmPage: Dmn, val dmnPxPage: Dmn, val dirOut: String, val colorBg: Color) {
    fun pxFromMm(v: Int) = (v * (dmnPxPage.x / dmnMmPage.x.toDouble())).toInt()
    fun mmFromPx(v: Int) = (v * (dmnMmPage.x /dmnPxPage.x.toDouble())).toInt()
    fun dmnPxFromDmnMm(dmn: Dmn) = Dmn(pxFromMm(dmn.x), pxFromMm(dmn.y))

    lateinit var page: Image
    var g: Graphics2D by Delegates.notNull()
    var numCard = 0
    var numPage = 0
    var cardset: Cardset? = null

    fun dmnPxCard() = dmnPxFromDmnMm(cardset!!.dmnCard)

    fun generate(cardsets: List<Cardset>) {
        File(dirOut).mkdirs()
        File(dirOut).listFiles().forEach { it.delete() }

        for (cs in cardsets) {
            val needStartPage = cardset==null || cs.dmnCard != cardset?.dmnCard || cs.dmnGrid != cardset?.dmnGrid
            cardset = cs
            if(needStartPage) startPage()
            cs.build(AideCardset(this, cs))
        }
        end()
    }

    fun startPage() {
        val cs = cardset!!
        numCard = 0
        numPage += 1
        page = Image(dmnPxPage.x, dmnPxPage.y, Image.TYPE_INT_RGB)
        g = page.createGraphics()
        g.setRenderingHint(
                        RenderingHints.KEY_TEXT_ANTIALIASING,
                        RenderingHints.VALUE_TEXT_ANTIALIAS_ON)
        g.color = colorBg
        g.fillRect(0, 0, dmnPxPage.x, dmnPxPage.y)
        g.color = invert(colorBg)
        val sm = pxFromMm(100)
        g.fillRect(pxFromMm(10),pxFromMm(10), sm, sm)
        g.font = g.font.deriveFont(88f)
        val dmnPxCard = dmnPxCard()
        g.drawString("${cs.dmnCard.x} x ${cs.dmnCard.y} mm/10", 2 * sm, sm)
        g.drawString("${dmnPxCard.x} x ${dmnPxCard.y} px", 10 * sm, sm)
        val xStart = (dmnPxPage.x - (dmnPxCard.x * cs.dmnGrid.x + (cs.dmnGrid.x - 1) * zazor)) / 2
        val yStart = (dmnPxPage.y - (dmnPxCard.y * cs.dmnGrid.y + (cs.dmnGrid.y - 1) * zazor)) / 2
        g.translate(xStart, yStart)
    }

    fun addCard(draw: (Graphics2D) -> Unit) {
        val cs = cardset!!
        val dmnPxCard = dmnPxCard()
        val gCard = g.create() as Graphics2D
        val xgCard = numCard % cs.dmnGrid.x
        val ygCard = numCard / cs.dmnGrid.x
        gCard.translate(xgCard * (dmnPxCard.x + zazor), ygCard * (dmnPxCard.y + zazor))
        draw(gCard)
        gCard.dispose()
        numCard += 1
        if (numCard == cs.dmnGrid.x * cs.dmnGrid.y) {
            writePage()
            startPage()
        }
    }

    private fun writePage() {
        saveJpg(page, File(dirOut, "page" + strNumPage() + ".jpg"))
    }

    private fun end() {
        if (numCard > 0) writePage()
    }

    fun strNumPage(): String {
        val s = numPage.toString()
        return if (s.length == 1) "0" + s else s
    }

    companion object {
        val zazor = 2
    }

    private fun invert(color: Color) = Color(255 - color.red, 255 - color.green, 255 - color.blue)

}

data class Dmn(val x: Int, val y: Int)

class Cardset(val name: String,val dmnCard: Dmn,val dmnGrid: Dmn, val build: AideCardset.() -> Unit)

class Card(val g: Graphics2D,val pager:Pager) {
    fun txtAttr(x: Int, y: Int,wrapX:Int,wrapY:Int, txtAttr: AttributedString) {
        val measurer = LineBreakMeasurer(txtAttr.iterator, g.fontRenderContext)
        val xCur = pager.pxFromMm(x).toFloat()
        var yCur = pager.pxFromMm(y).toFloat()
        val xWrapPx = pager.pxFromMm(wrapX).toFloat()
        val yWrapPx = pager.pxFromMm(wrapY).toFloat()
        val rows = ArrayList<TextLayout>()
        var yr = 0F
        while (measurer.position < txtAttr.iterator.endIndex){
            val layout = measurer.nextLayout(xWrapPx)
            rows.add(layout)
            yr += layout.ascent + layout.descent + layout.leading
        }
        yCur += (yWrapPx-yr)/2
        for (row in rows) {
            yCur += row.ascent
            drawTextLayout(g,row,xCur+(xWrapPx-row.bounds.width.toFloat())/2,yCur,Color.BLACK)
            yCur += row.descent + row.leading
        }
    }

    fun txtAttr(x: Int, y: Int,wrapX:Int,wrapY:Int, txtsAttr: List<AttributedString>) {
        val xCur = pager.pxFromMm(x).toFloat()
        var yCur = pager.pxFromMm(y).toFloat()
        val xWrapPx = pager.pxFromMm(wrapX).toFloat()
        val yWrapPx = pager.pxFromMm(wrapY).toFloat()
        val listRows = ArrayList<ArrayList<TextLayout>>()
        var prgMrgn:Float? = null
        var yr = 0F
        for ((idx,txtAttr) in txtsAttr.withIndex()) {
            val rows = ArrayList<TextLayout>()
            val measurer = LineBreakMeasurer(txtAttr.iterator, g.fontRenderContext)
            while (measurer.position < txtAttr.iterator.endIndex) {
                val layout = measurer.nextLayout(xWrapPx)
                rows.add(layout)
                yr += layout.ascent + layout.descent + layout.leading
                if(prgMrgn==null) prgMrgn = layout.ascent/4
            }
            if(idx==txtsAttr.lastIndex) yr += prgMrgn?:0F
            listRows.add(rows)
        }
        yCur += (yWrapPx-yr)/2
        for (rows in listRows) {
            for (row in rows) {
                yCur += row.ascent
                drawTextLayout(g, row, xCur + (xWrapPx - row.bounds.width.toFloat()) / 2, yCur, Color.BLACK)
                yCur += row.descent + row.leading
            }
            yCur += prgMrgn?:0F
        }
    }

    private fun drawTextLayout(g:Graphics2D, layout: TextLayout, x:Float, y:Float, color:Color){

        g.paint = Color(255,255,255,105)
        splashTextLayout(g,layout,x,y,9)

        g.paint = Color(255,255,255,180)
        splashTextLayout(g,layout,x,y,6)

        g.paint = Color.white
        splashTextLayout(g,layout,x,y,3)

        g.paint = color
        layout.draw(g, x, y);
    }

    private fun splashTextLayout(g:Graphics2D, layout: TextLayout, x:Float, y:Float, d:Int){
        layout.draw(g, x+d, y+d);
        layout.draw(g, x+d, y-d);
        layout.draw(g, x-d, y+d);
        layout.draw(g, x-d, y-d);

        layout.draw(g, x+d, y);
        layout.draw(g, x, y-d);
        layout.draw(g, x, y+d);
        layout.draw(g, x-d, y);
    }
}

fun cardset(name: String, dmnCard: Dmn,dmnGrid: Dmn,build: AideCardset.() -> Unit): Cardset {
    return Cardset(name, dmnCard,dmnGrid,build)
}
