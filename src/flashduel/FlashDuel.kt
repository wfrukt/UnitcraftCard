package flashduel

import crop
import resize
import rotate
import rotateBack
import saveJpg
import java.awt.BasicStroke
import java.awt.Color
import java.awt.Graphics2D
import java.awt.TexturePaint
import java.awt.geom.Rectangle2D
import java.io.File
import javax.imageio.ImageIO
import kotlin.properties.Delegates
import java.awt.image.BufferedImage as Image

fun main(args: Array<String>) {
    val dirSrc = File("data/flashduel/")
    val pager = Pager("output/flashduel/", Color.RED)
    for (file in dirSrc.listFiles { f -> f.name.endsWith("_profile.jpg") }) {
        val title = 265 to 30
        val imgAblsDraft = Image(title.first, 150 * 2 + 110 + title.second, Image.TYPE_INT_ARGB)
        val gAbl = imgAblsDraft.createGraphics()
        repeat(3) {
            val y = 150 * it
            val img = ImageIO.read(File(dirSrc, file.name.removeSuffix("_profile.jpg") + "_ability${it + 1}.jpg"))
            gAbl.drawImage(img,
                    0, y, title.first, y + title.second,
                    25, 25, 20 + title.first, 25 + title.second,
                    null)
            val abl = title.first to 110
            gAbl.drawImage(img,
                    0, y + title.second, abl.first, y + title.second + abl.second,
                    25, 304, 20 + abl.first, 304 + abl.second,
                    null)
        }
        val k = Pager.ydmnCard / imgAblsDraft.height.toDouble()
        val imgAbls = resize(imgAblsDraft, Math.round(imgAblsDraft.width * k).toInt(), Pager.ydmnCard, true,null)
        val mrgn = 25
        val imgBg = ImageIO.read(file)
        val imgCard = resize(crop(ImageIO.read(file), mrgn, mrgn, imgBg.width - mrgn, imgBg.height - mrgn), Pager.xdmnCard, Pager.ydmnCard, true)
        val gCard = imgCard.createGraphics()
        gCard.drawImage(imgAbls, imgCard.width - imgAbls.width, 0, null)
        pager.addCard(imgCard)
    }
    for (file in dirSrc.listFiles { f -> f.name.startsWith("dragon") }) {
        val imgCard = Image(431, 585, Image.TYPE_INT_ARGB)
        val imgBg = ImageIO.read(file)
        val g = imgCard.createGraphics()
        g.drawImage(imgBg,
                0, 0, imgCard.width, imgCard.height,
                21, 32, 21 + imgCard.width, 32 + imgCard.height,
                null)

        pager.addCard(resize(rotateBack(imgCard), Pager.xdmnCard, Pager.ydmnCard, false))
    }

    val imgDark = ImageIO.read(File(dirSrc,"woodDark.jpg"))
    val imgLight = ImageIO.read(File(dirSrc,"woodLight.jpg"))
    val xrWood = Pager.xdmnCard*2/9
    val yrWood = Pager.ydmnCard
    fun drawWood(g:Graphics2D,isDark:Boolean,num:Int){
        val wood = if(isDark) imgDark else imgLight
        g.paint = TexturePaint(wood, Rectangle2D.Double(0.0, 0.0, wood.width.toDouble(), wood.height.toDouble()))
        g.fillRoundRect(0, 0, xrWood, yrWood, 50, 50)
        g.stroke = BasicStroke(10F)
        g.paint = Color.white
        g.drawRoundRect(0, 0, xrWood, yrWood, 50, 50)
        g.font = g.font.deriveFont(180f)
        val wood2 = if(!isDark) imgDark else imgLight
        g.paint = TexturePaint(wood2, Rectangle2D.Double(0.0, 0.0, wood2.width.toDouble(), wood2.height.toDouble()))
        g.drawStringCenter(num.toString(),0,yrWood-xrWood,xrWood,xrWood)
        val tr = g.transform
        g.rotate(Math.PI/2+Math.PI/2)
        g.translate(-xrWood,-xrWood)
        g.drawStringCenter((19-num).toString(),0,0,xrWood,xrWood)
        g.transform = tr
        g.translate(xrWood,0)
    }
    repeat (2) { idxC ->
        fun num(i:Int) = i
        val imgCard = Image(Pager.xdmnCard, Pager.ydmnCard, Image.TYPE_INT_ARGB)
        val g = imgCard.createGraphics()
        g.color = Color.BLACK
        g.fillRect(0,0,Pager.xdmnCard, Pager.ydmnCard)
        drawWood(g,true,num(1))
        drawWood(g,true,num(2))
        repeat(2) {idx ->
            drawWood(g,false,num(3+idx))
        }
        drawWood(g,true,num(5))
        pager.addCard(imgCard)
    }
    repeat (2) {
        val imgCard = Image(Pager.xdmnCard, Pager.ydmnCard, Image.TYPE_INT_ARGB)
        val g = imgCard.createGraphics()
        g.color = Color.BLACK
        g.fillRect(0,0,Pager.xdmnCard, Pager.ydmnCard)
        drawWood(g,true,10)
        repeat(2) {idx ->
            drawWood(g,false,11+idx)
        }
        repeat(2) {idx ->
            drawWood(g,true,13+idx)
        }
        pager.addCard(imgCard)
    }
    pager.end()
}

fun Graphics2D.drawStringCenter(text: String,x:Int,y:Int, w:Int,h:Int) {
    val metrics = getFontMetrics(font)
    val xx = x+(w - metrics.stringWidth(text)) / 2
    val yy = y+(h - metrics.height) / 2 + metrics.ascent
    drawString(text, xx, yy)
}

class Pager(val dirOut: String, val colorBg: Color) {
    var page: Image by Delegates.notNull()
    var g: Graphics2D by Delegates.notNull()
    var numCard = xg * yg
    var numPage = 0

    init {
        File(dirOut).mkdirs()
        File(dirOut).listFiles().forEach { it.delete() }
        startPage()
    }

    fun startPage() {
        numCard = 0
        numPage += 1
        page = Image(xdmn, ydmn, Image.TYPE_INT_RGB)
        g = page.createGraphics()
        g.color = colorBg
        g.fillRect(0, 0, xdmn, ydmn)
        g.color = invert(colorBg)
        g.fillRect(10, 10, xdmnSm, ydmnSm)
        g.font = g.font.deriveFont(88f)
        g.drawString("$xdmnMmCard x $ydmnMmCard mm", 2 * xdmnSm, xdmnSm)
        g.drawString("$xdmnCard x $ydmnCard px", 8 * xdmnSm, xdmnSm)
        g.translate(xStart, yStart)
    }

    fun addCard(img: Image) {
        val gCard = g.create() as Graphics2D
        val xgCard = numCard % xg
        val ygCard = numCard / xg
        gCard.translate(xgCard * (xdmnCard + zazor), ygCard * (ydmnCard + zazor))
        //        card.draw(gCard,xdmnCard,ydmnCard,noCrop)
        //        gCard.drawImage(resize(card.image, xdmnCard, ydmnCard, noCrop), 0, 0, null)
        if (img.width != xdmnCard || img.height != ydmnCard) throw Exception("img has invalid dmn")
        gCard.drawImage(img, 0, 0, null)
        gCard.dispose()
        numCard += 1
        if (numCard == xg * yg) {
            writePage()
            startPage()
        }
    }

    fun writePage() {
        saveJpg(rotate(page), File(dirOut + "page" + strNumPage() + ".jpg"))
    }

    fun end() {
        if (numCard > 0) writePage()
    }

    fun strNumPage(): String {
        val s = numPage.toString()
        return if (s.length == 1) "0" + s else s
    }

    companion object {
        val xdmnMmCard = 100
        val ydmnMmCard = 73
        val xg = 4
        val yg = 4
        val zazor = 2
        val xdmnMm = 406.0
        val ydmnMm = 305.0
        val xdmn = 4795
        val ydmn = 3602
        val xdmnSm = (10 * (xdmn / xdmnMm)).toInt()
        val ydmnSm = (10 * (ydmn / ydmnMm)).toInt()
        val xdmnCard = (xdmnMmCard * (xdmn / xdmnMm)).toInt()
        val ydmnCard = (ydmnMmCard * (ydmn / ydmnMm)).toInt()

        val xStart = (xdmn - (xdmnCard * xg + (xg - 1) * zazor)) / 2
        val yStart = (ydmn - (ydmnCard * yg + (yg - 1) * zazor)) / 2
    }

    private fun invert(color: Color) = Color(255 - color.red, 255 - color.green, 255 - color.blue)
}