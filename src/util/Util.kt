import com.mortennobel.imagescaling.DimensionConstrain
import com.mortennobel.imagescaling.ResampleFilters
import com.mortennobel.imagescaling.ResampleOp
import java.awt.Color
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.IIOImage
import javax.imageio.ImageIO
import javax.imageio.ImageWriteParam
import javax.imageio.stream.FileImageOutputStream

fun rotate(img: BufferedImage): BufferedImage {
    val width = img.width
    val height = img.height
    val imgFlip = BufferedImage(height, width, img.type)
    for ( i in  0..width - 1) for (j in 0..height - 1)
        imgFlip.setRGB(height - 1 - j, i, img.getRGB(i, j))
    return imgFlip
}

fun saveJpg(img: BufferedImage, file: File) {
    val jpgWriter = ImageIO.getImageWritersByFormatName("jpg").next()
    val jpgWriteParam = jpgWriter.defaultWriteParam
    jpgWriteParam.compressionMode = ImageWriteParam.MODE_EXPLICIT
    jpgWriteParam.compressionQuality = 1.0f

    val outputStream = FileImageOutputStream(file)
    jpgWriter.output = outputStream
    val outputImage = IIOImage(img, null, null)
    jpgWriter.write(null, outputImage, jpgWriteParam)
    jpgWriter.dispose()
}

fun resize(img: BufferedImage, xdmn: Int, ydmn: Int, noCrop: Boolean,colorBg:Color?=Color.BLACK): BufferedImage {
    if(img.width==xdmn && img.width==ydmn) return img
    val fct = if (noCrop)
        Math.min(xdmn.toFloat() / img.width, ydmn.toFloat() / img.height)
    else
        Math.max(xdmn.toFloat() / img.width, ydmn.toFloat() / img.height)
    val op = ResampleOp(DimensionConstrain.createRelativeDimension(fct))
    op.filter = ResampleFilters.getLanczos3Filter()
    val imgRsz = op.filter(img, null)
    val imgRez = BufferedImage(xdmn, ydmn, BufferedImage.TYPE_INT_ARGB)
    val g = imgRez.createGraphics()
    if(colorBg!=null) {
        g.color = Color.BLACK
        g.fillRect(0, 0, xdmn, ydmn)
    }
    val x = (xdmn - imgRsz.width) / 2
    val y = (ydmn - imgRsz.height) / 2
    g.drawImage(imgRsz, x, y, null)
    return imgRez
}

fun rotateBack(img: BufferedImage): BufferedImage {
    val width = img.width
    val height = img.height
    val imgFlip = BufferedImage(height, width, img.type)
    for ( i in  0..width - 1) for (j in 0..height - 1)
        imgFlip.setRGB(j, width - 1 - i, img.getRGB(i, j))
    return imgFlip
}

fun crop(img: BufferedImage,x:Int,y:Int,w:Int,h:Int): BufferedImage {
    val imgRez = BufferedImage(w,h,BufferedImage.TYPE_INT_RGB)
    val g = imgRez.createGraphics()
    g.drawImage(img,0,0,w,h,x,y,x+w,y+h,null)
    return imgRez
}