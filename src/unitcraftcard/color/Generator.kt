package unitcraftcard.color

import com.mortennobel.imagescaling.DimensionConstrain
import com.mortennobel.imagescaling.ResampleFilters
import com.mortennobel.imagescaling.ResampleOp
import resize
import rotate
import saveJpg
import java.awt.BasicStroke
import java.awt.Color
import java.awt.Graphics2D
import java.awt.TexturePaint
import java.awt.geom.Rectangle2D
import java.io.File
import javax.imageio.IIOImage
import javax.imageio.ImageIO
import javax.imageio.ImageWriteParam
import javax.imageio.stream.FileImageOutputStream
import kotlin.properties.Delegates
import java.awt.image.BufferedImage as Image

//fun generate(data: String, noCrop: Boolean = false) {
//    val cards = File("data/$data").listFiles { it.isFile() }!!.map { Card(it) }
//    val pager = Pager("output/$data/")
//    for (card in cards) repeat(card.qnt) { pager.addCard(card) }
//    pager.end()
//}

fun generate(data: String, vararg cards: InfoCard) {
    generate(data,Color.white,*cards)
}

fun generate(data: String,colorBg:Color, vararg cards: InfoCard) {
    val pager = Pager("output/$data/",colorBg)
    for (card in cards.flatMap{it.cards()}) repeat(card.qnt) { pager.addCard(card.image) }
    pager.end()
}

data class ImgCard(val image: Image, val qnt: Int)

class Pager(val dirOut: String,val colorBg:Color) {
    var page: Image by Delegates.notNull()
    var g: Graphics2D by Delegates.notNull()
    var numCard = xg * yg
    var numPage = 0

    init {
        File(dirOut).mkdirs()
        File(dirOut).listFiles().forEach { it.delete() }
        startPage()
    }

    fun startPage() {
        numCard = 0
        numPage += 1
        page = Image(xdmn, ydmn, Image.TYPE_INT_RGB)
        g = page.createGraphics()
        g.color = colorBg
        g.fillRect(0, 0, xdmn, ydmn)
        g.color = invert(colorBg)
        g.fillRect(10, 10, xdmnSm, ydmnSm)
        g.font = g.font.deriveFont(88f)
        g.drawString("$xdmnMmCard x $ydmnMmCard mm", 2 * xdmnSm, xdmnSm)
        g.drawString("$xdmnCard x $ydmnCard px", 8 * xdmnSm, xdmnSm)
        g.translate(xStart, yStart)
    }

    fun addCard(img: Image) {
        val gCard = g.create() as Graphics2D
        val xgCard = numCard % xg
        val ygCard = numCard / xg
        gCard.translate(xgCard * (xdmnCard + zazor), ygCard * (ydmnCard + zazor))
        //        card.draw(gCard,xdmnCard,ydmnCard,noCrop)
        //        gCard.drawImage(resize(card.image, xdmnCard, ydmnCard, noCrop), 0, 0, null)
        if (img.width != xdmnCard || img.height != ydmnCard) throw Exception("img has invalid dmn")
        gCard.drawImage(img, 0, 0, null)
        gCard.dispose()
        numCard += 1
        if (numCard == xg * yg) {
            writePage()
            startPage()
        }
    }

    fun writePage() {
        saveJpg(rotate(page), File(dirOut + "page" + strNumPage() + ".jpg"))
    }

    fun end() {
        if (numCard > 0) writePage()
    }

    fun strNumPage(): String {
        val s = numPage.toString()
        return if (s.length == 1) "0" + s else s
    }

    companion object {
        val xdmnMmCard = 63.0
        val ydmnMmCard = 88.0
        val xg = 6
        val yg = 3
        val zazor = 2
        val xdmnMm = 406.0
        val ydmnMm = 305.0
        val xdmn = 4795
        val ydmn = 3602
        val xdmnSm = (10 * (xdmn / xdmnMm)).toInt()
        val ydmnSm = (10 * (ydmn / ydmnMm)).toInt()
        val xdmnCard = (xdmnMmCard * (xdmn / xdmnMm)).toInt()
        val ydmnCard = (ydmnMmCard * (ydmn / ydmnMm)).toInt()

        val xStart = (xdmn - (xdmnCard * xg + (xg - 1) * zazor)) / 2
        val yStart = (ydmn - (ydmnCard * yg + (yg - 1) * zazor)) / 2
    }

    private fun invert(color:Color) = Color(255-color.red,255-color.green,255-color.blue)
}

class Card(file: File) {
    val name: String
    val qnt: Int
    val image: Image

    init {
        qnt = qntFromFile(file)
        name = nameFromFile(file)
        image = ImageIO.read(file)
    }

    companion object {
        fun qntFromFile(file: File) =
                if (basename(file).indexOf('-') != -1) basename(file).split('-')[1].toInt() else 1

        fun nameFromFile(file: File) =
                if (basename(file).indexOf('-') != -1) basename(file).split('-')[0] else basename(file)

        fun basename(file: File) = file.name.replaceFirst("[.][^.]+$".toRegex(), "")
    }

    fun draw(g: Graphics2D, xdmn: Int, ydmn: Int, noCrop: Boolean) {
        g.drawImage(resize(image, xdmn, ydmn, noCrop), 0, 0, null)
    }
}

//object Seven {
//    platformStatic fun main(args: Array<String>) {
//        generate("seven", 64.0, 89.0, true)
//    }
//}

object Scott {
    @JvmStatic fun main(args: Array<String>) {
        val icons = mapOf(
                '$' to '\uf154',
                '#' to '\uf263',
                '@' to '\uf1c5',
                '!' to '\uf0e7',
                '?' to '\uf064',
                '|' to '\uf06d',
                '&' to '\uf0c4'
        )
        val bagpipes = InfoCard("scott", "bagpipe", icons) {
            bground(raw[0])

            drawTxt(40, 30, 560, raw[1], 52)
            drawTxt(40, 700, 560, raw[2], 46)
        }
        val waves = InfoCard("scott", "enemy", icons) {
            rotate()
            bground(raw[0])
            drawTxt(40, 30, 999, raw[1], 100)
            val dx = if(raw[2].length ==2) 680 else 770
            drawTxt(dx, 450, 999, raw[2], 200)
            val w = if(raw[2].length ==2) 700 else 740
            drawTxt(40, 480, w, raw[3], 46)
        }
        val tokens = InfoCard("scott", "token", icons) {
            bground(raw[0])
            drawTxt(40, 30, 560, raw[1], 52)
            drawTxt(40, 650, 560, raw[2], 46)
        }
        val side = InfoCard("scott", "side", icons) {
            bground(raw[0])
            qnt = raw[1].toInt()
        }
        val dagger = InfoCard("scott", "dagger", icons) {
            bground(raw[0])
            qnt = raw[1].toInt()
            drawTxt(40, 30, 999, raw[2], 200)
        }

        generate("scott", bagpipes, waves,tokens,side,dagger)
    }
}

object Exceed {
    @JvmStatic fun main(args: Array<String>) {
        val cards = InfoCard("exceed", "card",emptyMap()) {
            bground(raw[0])
            qnt = raw[1].toInt()
            if(raw[0]=="nehtaliHeavenPunishment"){
                drawTxt(30,580, 580, "Before: +2 Power for each card in opponent's Gauge", 34)
                drawTxt(30,630, 580, " After: Move 1", 34)
            }
        }

        generate("exceed",Color.green, cards)
    }
}

object ExceedRu {
    @JvmStatic fun main(args: Array<String>) {
        val icons = mapOf(
                '%' to '\uf263'
        )
        val cards = InfoCard("exceedRu", "base",icons) {
            fun drawStat(num:Int,color:Color,title:String){
                val stat = raw[2+num]
                if(stat!="-") {
                    drawTxt(30, 70 + num * 80, 580, stat, 86, color)
                    drawTxt(30,140+num*80, 580, title, 18, color)
                }
            }
            bground(raw[0])
            drawTxt(30, 10, 580, raw[1], 64)

            drawStat(0,Color.yellow.darker(),"скорость")
            drawStat(1,Color.blue.darker(),"дальность")
            drawStat(2, Color.red.darker(),"сила")
            drawStat(3, Color.MAGENTA.darker(),"броня")
            drawStat(4, Color.GREEN.darker(),"блок")

            drawTxt(30,550, 580, raw[7], 34)

            drawTxt(30,680, 580, raw[9], 50)
            drawTxt(70,680, 580, raw[8], 50)
            drawTxt(30,750, 580, raw[10], 34)

            qnt = 1//raw[8].toInt()
        }

        generate("exceedRu",Color.green, cards)
    }
}

object Codex {

    @JvmStatic fun main(args: Array<String>) {
        val building = InfoCard("codex", "building",emptyMap()) {
            g.paint = Color.black
            g.fillRect(0,0,xr(),yr())
            if(raw[0].endsWith("Base")){
                val road = loadImg("road")
                val xrStep = xr()/4
                val yrStep = yr()/5
                repeat(20){
                    val xg = it%4
                    val yg = it/4
                    g.paint = TexturePaint(road, Rectangle2D.Double(0.0, 0.0, road.width.toDouble(), road.height.toDouble()))
                    g.fillRoundRect(xg*xrStep,yg*yrStep,xrStep,yrStep,xrStep/3,xrStep/3)
                    g.stroke = BasicStroke(10F)
                    g.paint = Color.white
                    g.drawRoundRect(xg*xrStep,yg*yrStep,xrStep,yrStep,xrStep/3,xrStep/3)
                    g.font = g.font.deriveFont(xrStep*0.7F)
                    val w = g.getFontMetrics(g.font).stringWidth((it+1).toString())
                    g.drawString((it+1).toString(),xg*xrStep+(xrStep-w)/2,yg*yrStep+((yrStep-xrStep*0.7F)/2+xrStep*0.7F*0.8).toInt())
                }
                qnt = raw[1].toInt()
            }else if("T2" in raw[0]) {
                val building = loadImg(raw[0])
                val k = building.width / building.height.toDouble()

                val spec = loadImg(raw[1])
                val kSpec = spec.width / spec.height.toDouble()
                val xrSpec = xr()
                g.drawImage(resize(spec, xrSpec, (xrSpec / kSpec).toInt(), false), 0, yr() - (xrSpec / kSpec).toInt(), null)
                g.drawImage(resize(building, xr(), (xr() / k).toInt(), false), 0, 0, null)
                qnt = 1
            }else if("T1" in raw[0] || "T3" in raw[0]){
                val building = loadImg(raw[0])
                val k = building.width / building.height.toDouble()
                g.drawImage(resize(building, xr(), (xr() / k).toInt(), false), 0, 0, null)
                qnt = raw[1].toInt()
            }else if(raw[0]=="patrol") {
                var ySm = 0
                repeat(5) {
                    val img = loadImg(raw[0]+(it+1))
                    val k = img.width / img.height.toDouble()
                    g.drawImage(resize(img, xr(), (xr() / k).toInt(), false), 0, ySm, null)
                    ySm += (xr() / k).toInt()+5
                }
                qnt = raw[1].toInt()
            }else{
                bground(raw[0])
                qnt = raw[1].toInt()
            }
        }
        val neutral = InfoCard("codex", "neutral",emptyMap()) {
            bground(raw[0])
            qnt = raw[1].toInt()
        }

        generate("codex",Color.red.darker().darker(), building
                ,neutral
        )
    }
}

object Crim{
    @JvmStatic fun main(args: Array<String>) {
        val instrument = InfoCard("crim", "орудия",emptyMap()) {
            bground(raw[0])
            drawTxt(30, 10, 580, raw[0], 64)
        }
        val evidence = InfoCard("crim", "улики",emptyMap()) {
            bground(raw[0])
            drawTxt(30, 10, 580, raw[0], 64)
        }

        val planshets = InfoCard("crim", "планшеты",emptyMap()) {
            bground("планшет")
            drawTxt(30, 10, 580, raw[0], 70)
            repeat(6){
                drawTxt(30, 130+it*118, 580, raw[it+1], 64)
            }
        }

        generate("crim",Color.white, instrument,evidence, planshets)
    }
}