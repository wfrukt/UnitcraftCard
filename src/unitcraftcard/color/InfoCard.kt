package unitcraftcard.color

import resize
import rotateBack
import java.awt.*
import java.awt.font.LineBreakMeasurer
import java.awt.font.TextAttribute
import java.awt.font.TextLayout
import java.awt.geom.AffineTransform
import java.awt.geom.Point2D
import java.io.File
import java.text.AttributedString
import java.util.*
import javax.imageio.ImageIO
import java.awt.image.BufferedImage as Image

class InfoCard(val data: String, val name: String,val icons:Map<Char,Char>,val fn:Carder.()->Unit) {
    val dirSrc = File("data/$data/")
    val lines = File(dirSrc,"$name.txt").readLines()
    val cardsRaw = ArrayList<List<String>>()

    init {
        if(lines[0].isBlank()) throw Exception("1st line is blank")
        var linesCard = ArrayList<String>()
        for (line in lines) {
            if (line.isNotBlank()) linesCard.add(line)
            else {
                if(linesCard.isNotEmpty()) cardsRaw.add(linesCard)
                linesCard = ArrayList<String>()
            }
        }
        if(linesCard.isNotEmpty()) cardsRaw.add(linesCard)
    }

    fun cards()= cardsRaw.map{
        val c = Carder(it,data,icons)
                c.fn()
        c.card()

    }
}

class Carder(val raw:List<String>,val data: String,val icons:Map<Char,Char>){
    val dirSrc = File("data/$data/")
    private var isRotated = false
    var qnt = 1
    var img = Image(Pager.xdmnCard,Pager.ydmnCard, Image.TYPE_INT_ARGB)
    var g = createGraphics()

    fun xr() = img.width
    fun yr() = img.height

    fun part(idxRaw:Int,idxPart:Int) = raw[idxRaw].split(" ")[idxPart]

    fun createGraphics():Graphics2D{
        val g = img.createGraphics()
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON)
        return g
    }

    fun loadImg(name:String):Image {
        val png = File(dirSrc,"$name.png")
        val file = if(png.exists()) png else File(dirSrc,"$name.jpg")
        if(!file.exists()) throw Exception("$file not found")
        return ImageIO.read(file)
    }

    fun card() = ImgCard(if(isRotated) rotateBack(img) else img,qnt)

    fun drawImg(x:Int,y:Int,img:Image){
       g.drawImage(img,px(x),px(y),null)
    }

    fun rotate(){
        isRotated = true
        img = Image(Pager.ydmnCard,Pager.xdmnCard, Image.TYPE_INT_ARGB)
        g = createGraphics()
    }

    fun bground(nameImg:String){
        g.drawImage(resize(loadImg(nameImg), xr(), yr(), false),0,0,null)
    }

    companion object {
        fun px(v: Int) = (v * (Pager.xdmn / (Pager.xdmnMm * 10))).toInt()
        val fontAwe = Font.createFont(Font.TRUETYPE_FONT, File("data","fontawesome-webfont.ttf"))

    }

    private fun createAttrText(txt:String,sizeFont:Int):AttributedString{
        var txtRpl = txt
        val idxsAwe = ArrayList<Int>()
        for((key,icon) in icons) {
            idxsAwe.addAll(txt.indices.filter { txt[it] == key })
            txtRpl = txtRpl.replace(key,icon)
        }

        val attrText = AttributedString(txtRpl);
        val font  = Font("arial", Font.BOLD, sizeFont)
        attrText.addAttribute(TextAttribute.FONT, font);
        val fontIcons = fontAwe.deriveFont(Font.BOLD, sizeFont.toFloat())
        idxsAwe.forEach {
            attrText.addAttribute(TextAttribute.FONT, fontIcons, it, it + 1);

        }
        return attrText
    }

    fun drawTxt(x:Int, y:Int,wrap:Int, txt:String, sizeFont:Int,color:Color = Color.black){
        val attrText = createAttrText(txt,sizeFont)
        val measurer = LineBreakMeasurer(attrText.iterator, g.fontRenderContext);
        val xCur = px(x).toFloat()
        var yCur = px(y).toFloat()
        val wrapPx = px(wrap).toFloat()
        while (measurer.position < txt.length) {
            val layout = measurer.nextLayout(wrapPx);
            yCur += layout.ascent;
            drawTextLayout(g,layout,xCur,yCur,color)
            yCur += layout.descent + layout.leading;
        }
    }

    private fun drawTextLayout(g:Graphics2D,layout:TextLayout,x:Float,y:Float,color:Color){

        g.paint = Color(255,255,255,105)
        splashTextLayout(g,layout,x,y,9)

        g.paint = Color(255,255,255,180)
        splashTextLayout(g,layout,x,y,6)

        g.paint = Color.white
        splashTextLayout(g,layout,x,y,3)

        g.paint = color
        layout.draw(g, x, y);
    }

    private fun splashTextLayout(g:Graphics2D,layout:TextLayout,x:Float,y:Float,d:Int){
        layout.draw(g, x+d, y+d);
        layout.draw(g, x+d, y-d);
        layout.draw(g, x-d, y+d);
        layout.draw(g, x-d, y-d);

        layout.draw(g, x+d, y);
        layout.draw(g, x, y-d);
        layout.draw(g, x, y+d);
        layout.draw(g, x-d, y);
    }


}