package unitcraftcard.html.battlecon

import com.opencsv.CSVReader
import java.io.File
import java.io.InputStreamReader
import java.util.*

fun main(args: Array<String>) {
    generate("battlecon")
}


fun generate(dir: String) {
    val csv = CSVReader(InputStreamReader(File("data/$dir/cards.csv").inputStream(), Charsets.UTF_8))
    val cards = ArrayList<List<String>>()
    csv.readAll().filter { it[0].isNotEmpty() }.map { it.toList() }.forEach {
        cards.add(it)
        if (it[0] == "основа") cards.add(it)
    }
    val html = Html(cards)
    println(html.sb)
    File("output/$dir/").mkdirs()
    File("output/$dir/cards.html").writeText(html.sb.toString(), Charsets.UTF_8)
}

class Html(cards: List<List<String>>) {
    val sb = StringBuilder()

    init {
        sb.appendln(css)
        for ((i, card) in cards.withIndex()) {
            if (i % 9 == 0) div("table")
            if (i % 3 == 0) div("row")

            div("cell")
            addCard(card)
            endDiv()

            if ((i + 1) % 3 == 0) {
                endDiv()
            } else if (i == cards.lastIndex) {
                endDiv()
            }

            if ((i + 1) % 9 == 0) {
                endDiv()
            } else if (i == cards.lastIndex) {
                endDiv()
            }
        }
    }

    fun addCard(c: List<String>) {
        val isStyle = !c[1].endsWith("прием")
        block("title", c[2], isStyle)
        block("type", c[1], isStyle)
        if (c[3] != "" || c[4] != "" || c[5] != "" || (c[1].endsWith("стиль") || !c[1].startsWith("особый"))) {
            if (c[3] != "") param("скорость", c[3], isStyle) else param("&nbsp", "&nbsp", isStyle)
            if (c[4] != "") param("дальность", c[4], isStyle) else param("&nbsp", "&nbsp", isStyle)
            if (c[5] != "") param("сила", c[5], isStyle) else param("&nbsp", "&nbsp", isStyle)
        }
        block("desc", c[6])
        block("owner", c[0])
    }

    fun param(name: String, value: String, isStyle: Boolean) {
        val c = (if (!isStyle) value else "") + "<span class='paramTitle'>$name</span>" + (if (isStyle) value else "")
        block("param", c, isStyle)
    }

    fun block(cl: String, c: String, isAlignRight: Boolean = false) {
        div(cl + if (isAlignRight) " alignRight" else "")
        val text = c.replace("\n", if (cl == "desc") "<br><br>" else "<br>")
        sb.appendln(text)
        endDiv()
    }

    fun div(cl: String) {
        sb.appendln("<div class='$cl'>")
    }

    fun endDiv() {
        sb.appendln("</div>")
    }
}

val css = """<meta charset="utf-8"><style>""" + """
.type{
    font-size: 3.5mm;
    font-family: monospace;
}
.owner{
    padding-top: 2mm;
    font-size: 3.5mm;
    font-family: cursive;
}
.desc{
    font-size: 4.5mm;
}
.title{
    font-size: 8mm;
}

.alignRight{
    text-align: right;
}

.paramTitle{
    font-size: 5mm;
    padding: 3mm;
}

.param{
    font-size: 8mm;
}

.table{
    width: 192mm;
    display: table;
    border-collapse: collapse;
    table-layout:fixed;
    page-break-after: always;
}

.row{
    display: table-row;
}

.cell{
    width: 64mm;
    height: 89mm;
    display: table-cell;
    border: 1px solid black;
    padding-left: 3mm;
    padding-right: 3mm;
    overflow: hidden;
}

""" + "</style>"

