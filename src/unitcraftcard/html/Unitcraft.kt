package unitcraftcard.html

import com.opencsv.CSVReader
import java.io.File
import java.io.InputStreamReader
import java.util.*

val dir = "unitcraft"

fun main(args: Array<String>) {

    val csvUnits = readCsv("units").take(46)
    val csvLocations = readCsv("locations").take(8)
    val cards = ArrayList<List<String>>()
    csvUnits.forEach {
        cards.add(it)
        val qnt = try{it[4].toInt()}catch(ex:Exception){null}
        if(qnt!=null) repeat(qnt-1){ i ->
            cards.add(it)
        }
    }
    csvLocations.forEach {
        cards.add(it)
    }
    val html = Html(cards)
    File("output/$dir/").mkdirs()
    File("output/$dir/cards.html").writeText(html.sb.toString(), Charsets.UTF_8)
}

fun readCsv(name:String)=
    CSVReader(InputStreamReader(File("data/$dir/$name.csv").inputStream(), Charsets.UTF_8)).readAll().filter { it[0].isNotEmpty() }.map { it.toList() }


class Html(cards: List<List<String>>) {
    val sb = StringBuilder()

    init {
        sb.appendln(css)
        for ((i, card) in cards.withIndex()) {
            if (i % 9 == 0) div("table")
            if (i % 3 == 0) div("row")

            div("cell")
            addCard(card)
            endDiv()

            if ((i + 1) % 3 == 0) {
                endDiv()
            } else if (i == cards.lastIndex) {
                endDiv()
            }

            if ((i + 1) % 9 == 0) {
                endDiv()
            } else if (i == cards.lastIndex) {
                endDiv()
            }
        }
    }

    fun addCard(c: List<String>) {
        block("title",c[2].capitalize())
        block("type",c[1])
        block("desc",c[3])
        block("deck",c[0])
    }

    fun block(cl:String,c:String){
        div(cl)
        val text = c.replace("\n",if(cl=="desc") "<br><br>" else "<br>")
        sb.appendln(text)
        endDiv()
    }

    fun div(cl:String) {
        sb.appendln("<div class='$cl'>")
    }
    fun endDiv() {
        sb.appendln("</div>")
    }
    //    fun row(fn:()->Unit){
    //        sb.appendln("<div class='row'")
    //        fn()
    //        sb.appendln("</div")
    //    }
    //    fun endRow(){
    //
    //    }
    //    fun endRow(){
    //        sb.append("endRow")
    //    }
}

val css = """<meta charset="utf-8"><style>"""+"""
.type{
    font-size: 4mm;
    font-family: monospace;
}
.deck{
    padding-top: 2mm;
    font-size: 5mm;
    font-weight: bold;
}
.desc{
    font-size: 5mm;
}
.title{
    font-size: 8mm;
}

.table{
    width: 192mm;
    display: table;
    border-collapse: collapse;
    table-layout:fixed;
    page-break-after: always;
}

.row{
    display: table-row;
}

.cell{
    width: 64mm;
    height: 89mm;
    display: table-cell;
    border: 1px solid black;
    padding-left: 3mm;
    padding-right: 3mm;
    overflow: hidden;
}

"""+"</style>"