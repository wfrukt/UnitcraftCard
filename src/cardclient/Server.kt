package cardclient

import cardserver.Game
import org.json.simple.JSONObject
import org.json.simple.JSONValue

class Server(val update: (Snapshot) -> Unit) {

    val game = Game()

    init {
        fire()
    }

    fun fire() {
        val json = JSONValue.parse(game.snapshot()) as JSONObject
        update(Snapshot(json))
    }

    fun strike(idxCard: Int) {
        game.strike(idxCard)
        fire()
    }

    fun strikeEx(idxCard: Int) {
        game.strike(idxCard)
        fire()
    }

    fun strikeWild() {
        game.strikeWild()
        fire()
    }

    fun boost(idxCard: Int) {

    }

    fun prepare() {
        game.prepare()
        fire()
    }

    fun move(move: Move, pf: PayForce) {

    }

    fun exceed() {

    }

    fun forceDraw(pf: PayForce) {

    }

    fun updateState() {

    }

    fun reshuffle() {

    }
}


data class PayForce(val idxsDiscard: List<Int>, val qntGauge: Int) {
    val qnt = idxsDiscard.size + qntGauge
}

data class Move(val dir: Dir, val qnt: Int)

enum class Dir {
    fw, bw
}

class Snapshot(json: JSONObject) {
    val deck = jsonList(json["deck"])
    val discard = jsonList(json["discard"])
    val hand = jsonList(json["hand"])
    val board = jsonList(json["board"])

    private fun jsonList(json: Any?) = json as List<String>
}