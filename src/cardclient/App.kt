package cardclient

import javafx.application.Application;
import javafx.beans.value.ChangeListener
import javafx.beans.value.ObservableValue
import javafx.collections.FXCollections
import javafx.collections.ListChangeListener
import javafx.collections.ObservableList
import javafx.geometry.Insets
import javafx.scene.Scene;
import javafx.scene.control.*
import javafx.scene.layout.*
import javafx.scene.text.Font
import javafx.scene.text.FontWeight
import javafx.scene.text.Text
import javafx.stage.Stage
import java.util.*

class Window : Application() {

    val prepare = object : Action("Prepare"){
        override fun send() {
            server.prepare()
        }
        override fun update(){
            labelCmd.text = "Prepare"
        }
    }

    val strike = object : Action("Strike"){
        override fun send() {
            val idxCard = handView.selectionModel.selectedIndices.getOrNull(0)
            if(idxCard!=null) server.strike(idxCard)
        }

        override fun update(){
            labelCmd.text = "Strike - " + handView.selectionModel.selectedItems.getOrNull(0)
        }
    }

    val strikeEx = object : Action("Strike EX"){
        override fun send() {
            val idxCard = handView.selectionModel.selectedIndices.getOrNull(0)
            if(idxCard!=null) server.strikeEx(idxCard)
        }

        override fun update(){
            labelCmd.text = "Strike EX - " + handView.selectionModel.selectedItems.getOrNull(0)
        }
    }

    val strikeWild = object : Action("Wild Strike"){
        override fun send() {
            server.strikeWild()
        }

        override fun update(){
            labelCmd.text = "Wild Strike"
        }
    }

    val boost = object : Action("Boost"){
        override fun send() {
            server.boost(handView.selectionModel.selectedIndex)
        }

        override fun update(){
            labelCmd.text = "Boost - " + handView.selectionModel.selectedItems.getOrNull(0)
        }
    }

    val reshuffle = object : Action("Reshuffle"){
        override fun send() {
            server.reshuffle()
        }

        override fun update(){
            labelCmd.text = "Reshuffle"
        }
    }
    val move = object : Action("Move"){
        override fun send() {

        }

        override fun update(){
            labelCmd.text = "Move"
        }
    }
    val exceed = object : Action("Exceed"){
        override fun send() {
            server.exceed()
        }

        override fun update(){
            labelCmd.text = "Exceed"
        }
    }

    val actions = FXCollections.observableArrayList(strike,strikeEx,strikeWild,boost,reshuffle,prepare,move,exceed)
    val hand = FXCollections.observableArrayList<String>()
    val deck = FXCollections.observableArrayList<String>()
    val discard = FXCollections.observableArrayList<String>()
    val board = FXCollections.observableArrayList<String>()

    val server = Server(){
        hand.replace(it.hand)
        deck.replace(it.deck)
        discard.replace(it.discard)
        board.replace(it.board)
    }

    override fun start(primaryStage: Stage) {
        buildStage(primaryStage)
    }

    val actionsView = ListView<Action>()
    val handView = ListView<String>()

    val listenerSelectList = ChangeListener<cardclient.Action> { observable, oldValue, newValue -> updateCmd() }

    val listenerSelectListStr = ChangeListener<kotlin.String> { observable, oldValue, newValue -> updateCmd() }

    private fun addListenSelect(listView:ListView<String>){
        listView.selectionModel.selectedItemProperty().addListener(listenerSelectListStr)
    }

    private fun buildStage(primaryStage: Stage){
        actionsView.items = actions
        actionsView.selectionModel.selectedItemProperty().addListener(listenerSelectList)

        handView.items = hand
        handView.selectionModel.selectionMode = SelectionMode.MULTIPLE
        addListenSelect(handView)
        handView.setOnMouseClicked{
            updateCmd()
        }

        val deckView = ListView<String>()
        deckView.items = deck

        val discardView = ListView<String>()
        discardView.items = discard

        val vbox = VBox().apply {
            children.add(HBox().apply {
                children.add(titled(actionsView,"Actions"))
                children.add(titled(handView,"Hand"))
            })
            children.add(HBox().apply {
                children.add(titled(discardView,"Discard"))
                children.add(titled(deckView,"Deck"))
            })
        }

        val root = BorderPane()
        root.top = createBoard()
        root.center = vbox
        root.bottom = createToolbar()

        val scene = Scene(root, 600.0, 500.0)

        primaryStage.title = "Exceed"
        primaryStage.scene = scene
        primaryStage.show()
    }

    fun titled(control: Control,title:String): Pane {
        val vbox = VBox()
        vbox.children.add(Text(title).apply{
            VBox.setMargin(this, Insets(5.0, 0.0, 5.0, 5.0));
            font = Font.font("Arial", FontWeight.BOLD, 14.0)
        })
        vbox.children.add(control)
        return vbox
    }

    fun createBoard():Pane{
        val hbox = HBox()
        val pb = ArrayList<Button>()
        repeat(9){
            val text = Button()
            text.prefWidth = 100.0
            text.prefHeight = 100.0
            pb.add(text)
            hbox.children.add(text)
        }
        board.addListener(object:ListChangeListener<String>{
            override fun onChanged(c: ListChangeListener.Change<out String>) {
                updateBoard(pb)
            }
        })
        updateBoard(pb)
        return hbox
    }

    fun updateBoard(pb:ArrayList<Button>){
        for ((idx, it) in pb.withIndex()) {
            it.text = board.getOrNull(idx)?:""
        }
    }

    val labelCmd = Label()

    fun createToolbar():Pane{
        HBox.setMargin(labelCmd, Insets(5.0, 5.0, 5.0, 5.0));
        labelCmd.font = Font.font("Arial", FontWeight.BOLD, 14.0)
        val btnSend = Button("Send")
        btnSend.setOnAction{
            actionsView.selectionModel.selectedItem?.send()
        }
        return HBox().apply {
            children.add(labelCmd)
            children.add(btnSend)
        }
    }

    fun updateCmd(){
        actionsView.selectionModel.selectedItem?.update()
    }

    companion object{
        private fun ObservableList<String>.replace(list:List<String>){
            this.clear()
            this.addAll(list)
        }
    }
}

fun main(args: Array<String>) {
    Application.launch(Window::class.java,*args)
}

abstract class Action(val name:String){
    abstract fun send()
    abstract fun update()
    override fun toString() = name
}

